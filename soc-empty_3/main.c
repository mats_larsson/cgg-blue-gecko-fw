/***********************************************************************************************//**
 * \file   main.c
 * \brief  Silicon Labs Empty Example Project
 *
 * This example demonstrates the bare minimum needed for a Blue Gecko C application
 * that allows Over-the-Air Device Firmware Upgrading (OTA DFU). The application
 * starts advertising after boot and restarts advertising after a connection is closed.
 ***************************************************************************************************
 * <b> (C) Copyright 2016 Silicon Labs, http://www.silabs.com</b>
 ***************************************************************************************************
 * This file is licensed under the Silabs License Agreement. See the file
 * "Silabs_License_Agreement.txt" for details. Before using this software for
 * any purpose, you must agree to the terms of that agreement.
 **************************************************************************************************/

/* Board headers */
#include "boards.h"
#include "ble-configuration.h"
#include "board_features.h"

/* Bluetooth stack headers */
#include "bg_types.h"
#include "native_gecko.h"
#include "gatt_db.h"
#include "aat.h"

/* Libraries containing default Gecko configuration values */
#include "em_emu.h"
#include "em_cmu.h"
#ifdef FEATURE_BOARD_DETECTED
#include "bspconfig.h"
#include "pti.h"
#endif

/* Device initialization header */
#include "InitDevice.h"
#include "gpiointerrupt.h"
#ifdef FEATURE_SPI_FLASH
#include "em_usart.h"
#include "mx25flash_spi.h"
#endif /* FEATURE_SPI_FLASH */

/***********************************************************************************************//**
 * @addtogroup Application
 * @{
 **************************************************************************************************/

/***********************************************************************************************//**
 * @addtogroup app
 * @{
 **************************************************************************************************/

#ifndef MAX_CONNECTIONS
#define MAX_CONNECTIONS 1
#endif
uint8_t bluetooth_stack_heap[DEFAULT_BLUETOOTH_HEAP(MAX_CONNECTIONS)];

#ifdef FEATURE_PTI_SUPPORT
static const RADIO_PTIInit_t ptiInit = RADIO_PTI_INIT;
#endif

/* Gecko configuration parameters (see gecko_configuration.h) */
static const gecko_configuration_t config = {
  .config_flags = 0,
  .sleep.flags = SLEEP_FLAGS_DEEP_SLEEP_ENABLE,
  .bluetooth.max_connections = MAX_CONNECTIONS,
  .bluetooth.heap = bluetooth_stack_heap,
  .bluetooth.heap_size = sizeof(bluetooth_stack_heap),
  .bluetooth.sleep_clock_accuracy = 100, // ppm
  .gattdb = &bg_gattdb_data,
  .ota.flags = 0,
  .ota.device_name_len = 3,
  .ota.device_name_ptr = "OTA",
  #ifdef FEATURE_PTI_SUPPORT
  .pti = &ptiInit,
  #endif
};

/* Flag for indicating DFU Reset must be performed */
uint8_t boot_to_dfu = 0;

float lubricantType;
float maximumPressure;
float lubricationQuantity;
float totLubAmount;

uint8_t commandBytes[5] = {0,0,0,0,0};

// Flags
uint8_t isConnected = 0;
uint8_t batteryNotificationEnabled = 0;
uint8_t lubricationNotificationEnabled = 0;
uint8_t commandNotificationEnabled = 0;


void readButtonInit(void){
	GPIO_PinModeSet(BSP_GPIO_PB0_PORT, BSP_GPIO_PB0_PIN, gpioModeInput, 1);
	GPIO_ExtIntConfig(BSP_GPIO_PB0_PORT,BSP_GPIO_PB0_PIN,BSP_GPIO_PB0_PIN,false,true,true);
	GPIOINT_Init();
	GPIOINT_CallbackRegister(BSP_GPIO_PB0_PIN,gecko_external_signal);
}


/**
 * @brief  Main function
 */
void main(void)
{

#ifdef FEATURE_SPI_FLASH
  /* Put the SPI flash into Deep Power Down mode for those radio boards where it is available */
  MX25_init();
  MX25_DP();
  /* We must disable SPI communication */
  USART_Reset(USART1);

#endif /* FEATURE_SPI_FLASH */
  lubricantType = 0.0f;
  maximumPressure = 0.0f;
  lubricationQuantity = 0.0f;
  totLubAmount = 0.0f;
  uint8_t batState = 100;
  uint8_t batTimerCounter = 0;

  /* Initialize peripherals */
  enter_DefaultMode_from_RESET();

  /* Initialize stack */
  gecko_init(&config);

  while (1) {
    /* Event pointer for handling events */
    struct gecko_cmd_packet* evt;

    /* Check for stack event. */
    evt = gecko_wait_event();

    /* Handle events */
    switch (BGLIB_MSG_ID(evt->header)) {
    /* This boot event is generated when the system boots up after reset.
     * Here the system is set to start advertising immediately after boot procedure. */
    case gecko_evt_system_boot_id:
    	isConnected = 0;
    	batteryNotificationEnabled = 0;
    	lubricationNotificationEnabled = 0;
    	commandNotificationEnabled = 0;
    	/* Set advertising parameters. 100ms advertisement interval. All channels used.
    	 * The first two parameters are minimum and maximum advertising interval, both in
    	 * units of (milliseconds * 1.6). The third parameter '7' sets advertising on all channels. */
    	gecko_cmd_le_gap_set_adv_parameters(160, 160, 7);

    	/* Start general advertising and enable connections. */
    	gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
    	break;

    case gecko_evt_le_connection_opened_id:
    	isConnected = 1;
    	readButtonInit();
    	break;

    case gecko_evt_le_connection_closed_id:
    	isConnected = 0;
    	gecko_cmd_hardware_set_soft_timer(0, 0, 0);
    	batteryNotificationEnabled = 0;
    	lubricationNotificationEnabled = 0;
    	commandNotificationEnabled = 0;
    	/* Check if need to boot to dfu mode */
    	if (boot_to_dfu) {
    		/* Enter to DFU OTA mode */
    		gecko_cmd_system_reset(2);
    	} else {
    		/* Restart advertising after client has disconnected */
    		gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);
    	}
    	break;


    case gecko_evt_gatt_server_user_write_request_id:

    	if(evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_data_control_characteristic){
    		//send response OK
    		gecko_cmd_gatt_server_send_user_write_response(
    				evt->data.evt_gatt_server_user_write_request.connection,
					gattdb_data_control_characteristic,
					bg_err_success);

    		//Set floats from received data
    		uint8_t *pArray1;
    		pArray1 = (uint8_t *) &lubricantType;

    		uint8_t *pArray2;
    		pArray2 = (uint8_t *) &maximumPressure;

    		uint8_t *pArray3;
    		pArray3 = (uint8_t *) &lubricationQuantity;

    		for(uint8_t i = 0; i<4; i++){
    			*pArray1++ = evt->data.evt_gatt_server_user_write_request.value.data[i];
    			*pArray2++ = evt->data.evt_gatt_server_user_write_request.value.data[i+4];
    			*pArray3++ = evt->data.evt_gatt_server_user_write_request.value.data[i+8];
    		}

    		if(isConnected == lubricationNotificationEnabled == 1)
    		{
    			uint8_t sendBytes[16];
    			uint8_t lubricantTypeBytes[4];
    			uint8_t pressureBytes[4];
    			uint8_t quantityBytes[4];
    			uint8_t totBytes[4];

    			float2Bytes(lubricantType, &lubricantTypeBytes[0]);
    			float2Bytes(maximumPressure, &pressureBytes[0]);
    			float2Bytes(lubricationQuantity, &quantityBytes[0]);
    			float2Bytes(totLubAmount, &totBytes[0]);

    			for(uint8_t i = 0; i<4; i++)
    			{
    				sendBytes[i] = lubricantTypeBytes[i];
    				sendBytes[i+4] = pressureBytes[i];
    				sendBytes[i+8] = quantityBytes[i];
    				sendBytes[i+12] = totBytes[i];
    			}
    			gecko_cmd_gatt_server_send_characteristic_notification(0xFF, gattdb_lubrication_notif_characteristic, 16, &sendBytes);
    		}
    	}

    	if(evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_command_characteristic)
    	{
    		//send response OK
    		gecko_cmd_gatt_server_send_user_write_response(
    				evt->data.evt_gatt_server_user_write_request.connection,
					gattdb_command_characteristic,
					bg_err_success);

    		for(uint8_t i = 0; i<5; i++){
    			commandBytes[i] = evt->data.evt_gatt_server_user_write_request.value.data[i];
    		}

    		if(commandBytes[0] == 0x4F){
    			uint8_t diagnosticResponse[20];
    			parseDiagnosticDataArray(&diagnosticResponse[0]);

    			if(commandNotificationEnabled == 1)
    			{
    				gecko_cmd_gatt_server_send_characteristic_notification(0xFF, gattdb_command_response_characteristic, 20, diagnosticResponse);
    			}
    		}
    		else
    		{
    			if((commandNotificationEnabled == 1)){
    				gecko_cmd_gatt_server_send_characteristic_notification(0xFF, gattdb_command_response_characteristic, 5, commandBytes);
    			}
    		}
    	}

    	if (evt->data.evt_gatt_server_user_write_request.characteristic == gattdb_ota_control) {
    		/* Set flag to enter to OTA mode */
    		boot_to_dfu = 1;
    		/* Send response to Write Request */
    		gecko_cmd_gatt_server_send_user_write_response(
    				evt->data.evt_gatt_server_user_write_request.connection,
					gattdb_ota_control,
					bg_err_success);

    		/* Close connection to enter to DFU OTA mode */
    		gecko_cmd_endpoint_close(evt->data.evt_gatt_server_user_write_request.connection);
    	}
    	break;

    case gecko_evt_gatt_server_user_read_request_id:
    	//Keep this...
    	if (evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_battery_level)
    	{
    		batState -= 2;
    		if (batState <= 0){
    			batState = 100;
    		}
    		gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,evt->data.evt_gatt_server_user_read_request.characteristic,0,1,&batState);
    	}


    	if (evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_command_response_characteristic)
    	{
    		gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,evt->data.evt_gatt_server_user_read_request.characteristic,0,5,commandBytes);
    	}


    	if (evt->data.evt_gatt_server_user_read_request.characteristic == gattdb_lubrication_notif_characteristic)
    	{
    		uint8_t sendBytes[16];
    		uint8_t lubricantTypeBytes[4];
    		uint8_t pressureBytes[4];
    		uint8_t quantityBytes[4];
    		uint8_t totBytes[4];

    		float2Bytes(lubricantType, &lubricantTypeBytes[0]);
    		float2Bytes(maximumPressure, &pressureBytes[0]);
    		float2Bytes(lubricationQuantity, &quantityBytes[0]);
    		float2Bytes(totLubAmount, &totBytes[0]);

    		for(uint8_t i = 0; i<4; i++)
    		{
    			sendBytes[i] = lubricantTypeBytes[i];
    			sendBytes[i+4] = pressureBytes[i];
    			sendBytes[i+8] = quantityBytes[i];
    			sendBytes[i+12] = totBytes[i];
    		}
    		gecko_cmd_gatt_server_send_user_read_response(evt->data.evt_gatt_server_user_read_request.connection,gattdb_lubrication_notif_characteristic,0,16,sendBytes);
    	}
    	break;

    case gecko_evt_gatt_server_characteristic_status_id:

    	if ((evt->data.evt_gatt_server_characteristic_status.characteristic == gattdb_battery_level)
    			&& (evt->data.evt_gatt_server_characteristic_status.status_flags == 0x01))
    	{
    		if (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 0x01)
    		{
    			batteryNotificationEnabled = 1;
    			gecko_cmd_hardware_set_soft_timer(32768, 0, 0);
    		}
    		else if (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 0x00)
    		{
    			//Stop only if no other notification is running
    			if(lubricationNotificationEnabled == 0)
    			{
    				gecko_cmd_hardware_set_soft_timer(0, 0, 0);
    			}
    			batteryNotificationEnabled = 0;
    		}
    	}

    	if ((evt->data.evt_gatt_server_characteristic_status.characteristic == gattdb_lubrication_notif_characteristic)
    			&& (evt->data.evt_gatt_server_characteristic_status.status_flags == 0x01))
    	{
    		if (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 0x01)
    		{
    			lubricationNotificationEnabled = 1;
    		} else if (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 0x00)
    		{
    			lubricationNotificationEnabled = 0;
    		}
    	}

    	if ((evt->data.evt_gatt_server_characteristic_status.characteristic == gattdb_command_response_characteristic)
    			&& (evt->data.evt_gatt_server_characteristic_status.status_flags == 0x01))
    	{
    		if (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 0x01)
    		{
    			commandNotificationEnabled = 1;
    		} else if (evt->data.evt_gatt_server_characteristic_status.client_config_flags == 0x00)
    		{
    			commandNotificationEnabled = 0;
    		}
    	}
    	break;


    case gecko_evt_hardware_soft_timer_id:

    	if (batTimerCounter++ == 20)
    	{
    		batState -= 2;
    		if (batState <= 0)
    		{
    			batState = 100;
    		}

    		if(isConnected == batteryNotificationEnabled == 1)
    		{
    			gecko_cmd_gatt_server_send_characteristic_notification(0xFF, gattdb_battery_level, 1, &batState);
    		}
    		batTimerCounter = 0;
    	}
    	break;
    case gecko_evt_system_external_signal_id:
            	if(evt->data.evt_system_external_signal.extsignals == BSP_GPIO_PB0_PIN)
            	{
            					uint8_t sendBytes[16];
            		    		uint8_t lubricantTypeBytes[4];
            		    		uint8_t pressureBytes[4];
            		    		uint8_t quantityBytes[4];
            		    		uint8_t totBytes[4];

            		    		lubricantType++;
            		    		lubricationQuantity++;
            		    		maximumPressure++;
            		    		totLubAmount += lubricationQuantity;

            		    		float2Bytes(lubricantType, &lubricantTypeBytes[0]);
            		    		float2Bytes(maximumPressure, &pressureBytes[0]);
            		    		float2Bytes(lubricationQuantity, &quantityBytes[0]);
            		    		float2Bytes(totLubAmount, &totBytes[0]);

            		    		for(uint8_t i = 0; i<4; i++)
            		    		{
            		    			sendBytes[i] = lubricantTypeBytes[i];
            		    			sendBytes[i+4] = pressureBytes[i];
            		    			sendBytes[i+8] = quantityBytes[i];
            		    			sendBytes[i+12] = totBytes[i];
            		    		}

            		    		if(isConnected == lubricationNotificationEnabled == 1)
            		    		{
            		    			gecko_cmd_gatt_server_send_characteristic_notification(0xFF, gattdb_lubrication_notif_characteristic, 16, &sendBytes);
            		    		}
            	}
            	break;

    default:
    	break;
    }
  }
}


void parseDiagnosticDataArray(uint8_t* data_array){
	uint32_t svnRev = 54321;
	uint8_t svnBytes[4];
	unsignedInt2Bytes(svnRev, &svnBytes[0]);

	float batVolt = 3.78f;
	uint8_t batBytes[4];
	float2Bytes(batVolt, &batBytes[0]);

	uint32_t serialNr = 12345;
	uint8_t serialbytes[4];
	unsignedInt2Bytes(serialNr, &serialbytes[0]);

	uint32_t uptime = 99999;
	uint8_t timeBytes[4];
	unsignedInt2Bytes(uptime, &timeBytes[0]);

	for(int i = 0; i<4; i++){
		data_array[i+4] = svnBytes[i];
		data_array[i+8] = batBytes[i];
		data_array[i+12] = serialbytes[i];
		data_array[i+16] = timeBytes[i];
	}
	data_array[0] = 0x4F;
	data_array[1] = 0x00;
	data_array[2] = 0x00;
}

void float2Bytes(float val,uint8_t* bytes_array){
  // Create union of shared memory space
  union {
    float float_variable;
    uint8_t temp_array[4];
  } u;
  // Overite bytes of union with float variable
  u.float_variable = val;
  // Assign bytes to input array
  memcpy(bytes_array, u.temp_array, 4);
}

void unsignedInt2Bytes(uint32_t val,uint8_t* byte_array){
  // Create union of shared memory space
  union {
	  uint32_t int_variable;
    uint8_t temp_array[4];
  } u;
  // Overite bytes of union with float variable
  u.int_variable = val;
  // Assign bytes to input array
  memcpy(byte_array, u.temp_array, 4);
}

/** @} (end addtogroup app) */
/** @} (end addtogroup Application) */
